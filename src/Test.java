

public class Test {
    public static void main(String[] args) {
        long seed = 3207;
        MTRandom mt = new MTRandom(seed);   

        
        double sum=0;
        for (int i=0; i<1000000;i++) {
            sum += mt.negExp(10);
        }

        System.out.println(sum/1000000);
}
}