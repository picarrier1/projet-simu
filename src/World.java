import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

public class World {
    private List<Humain>[][] map;           // Tableau 2D de listes d'humains
    private int taille;                     // Taille de la grille
    private List<Humain> ListeSusceptibles; // Liste des agents susceptibles de devenir Exposed
    private int nbSusceptibles;             // Nombre d'agents susceptibles
    private int nbInfectes;                 // Nombre d'agents infectés
    private int nbRecovered;                // Nombre d'agents récupérés
    private int nbExposed;                  // Nombre d'agents exposés
    private List<Humain> individus;         // Liste de tous les individus dans le monde

    // Constructeur
    public World(int taille, List<Humain> individus) {
        // Initialisation des variables
        this.taille = taille;
        this.map = new ArrayList[taille][taille];
        this.individus = individus;

        // Initialisation de la carte avec des listes vides
        for (int i = 0; i < taille; i++) {
            for (int j = 0; j < taille; j++) {
                this.map[i][j] = new ArrayList<Humain>();
            }
        }
        
        this.ListeSusceptibles = new ArrayList<>();

        // Initialisation des compteurs
        this.nbSusceptibles = 0;
        this.nbInfectes = 0;
        this.nbRecovered = 0;
        this.nbExposed = 0;

        // Répartition des individus dans les listes initiales en fonction de leur statut
        for (Humain humain : individus) {
            int x = humain.getX();
            int y = humain.getY();
            this.map[x][y].add(humain);
            switch (humain.getStatut()) {
                case S:
                    ListeSusceptibles.add(humain);
                    this.nbSusceptibles++;
                    break;
                case I:
                    this.nbInfectes++;
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Méthode pour avancer d'un jour dans la simulation.
     * Gère les statuts des individus, leur déplacement et les expositions.
     */
    public void jourSuivant() {

        // Remettre les compteurs à 0
        this.nbSusceptibles = 0;
        this.nbInfectes = 0;
        this.nbRecovered = 0;
        this.nbExposed = 0;

        // Gestion des statuts et déplacement pour chaque individu
        for (Humain agent : this.individus) {
            Humain.Statuts temp = agent.getStatut();
            agent.incrementTempsStatut();
            if (agent.getStatut() == Humain.Statuts.S && temp == Humain.Statuts.R) {
                ListeSusceptibles.add(agent);
            }
            deplacement(agent);
            updateCounts(agent);
        }

        // Gestion de l'exposition des individus
        exposition();
    }

    /**
     * Met à jour les compteurs d'individus en fonction de leur nouveau statut.
     * @param agent L'individu à mettre à jour.
     */
    private void updateCounts(Humain agent) {
        switch (agent.getStatut()) {
            case S:
                this.nbSusceptibles++;
                break;
            case E: // Géré dans exposition()
                break;
            case I:
                this.nbInfectes++;
                break;
            case R:
                this.nbRecovered++;
                break;
        }
    }

    /**
     * Supprime un individu de la carte.
     * @param agent L'individu à supprimer.
     */
    private void removeAgent(Humain agent) {
        map[agent.getX()][agent.getY()].remove(agent);
    }

    /**
     * Ajoute un individu à la carte.
     * @param agent L'individu à ajouter.
     */
    private void addAgent(Humain agent) {
        map[agent.getX()][agent.getY()].add(agent);
    }

    /**
     * Déplace un individu sur la carte.
     * @param agent L'individu à déplacer.
     */
    private void deplacement(Humain agent) {
        removeAgent(agent);
        agent.newCord(taille);
        addAgent(agent);
    }

    /**
     * Gère l'exposition des individus susceptibles.
     */
    private void exposition() {
        Iterator<Humain> it = ListeSusceptibles.iterator();

        while (it.hasNext()) {
            Humain susceptible = it.next();
            int nbI = countNbI(susceptible);
            if (susceptible.peutDevenirE(nbI)) {
                it.remove();
                this.nbExposed++;
                this.nbSusceptibles--;
            }
        }
    }

    /**
     * Compte le nombre d'individus infectés autour d'un individu.
     * @param humain L'individu pour lequel compter les voisins infectés.
     * @return Le nombre d'individus infectés voisins.
     */
    private int countNbI(Humain humain) {
        int count = 0;
        int row = humain.getX();
        int col = humain.getY();
        
        for (int i = row - 1; i <= row + 1; i++) {
            for (int j = col - 1; j <= col + 1; j++) {
                int toroidal_i = toroidalIndex(i, taille);
                int toroidal_j = toroidalIndex(j, taille);
                List<Humain> listeDansCase = map[toroidal_i][toroidal_j];
                for (Humain humainMemeCase : listeDansCase) {
                    if (humainMemeCase.getStatut() == Humain.Statuts.I) {
                        count++;
                    }
                }
            }
        }
        
        if (humain.getStatut() == Humain.Statuts.I) {
            count--;
        }

        return count;
    }

    /**
     * Gère les index toroïdaux pour assurer la périodicité de la grille.
     * @param index L'index à ajuster.
     * @param maxIndex La taille maximale de la grille.
     * @return L'index ajusté.
     */
    private static int toroidalIndex(int index, int maxIndex) {
        if (index < 0) { return maxIndex + index; }
        else if (index >= maxIndex) { return index - maxIndex; }
        else { return index; }
    }

    public List<Humain> getTabS() {
        return this.ListeSusceptibles;
    }

    /**
     * Getter pour le nombre d'individus susceptibles.
     * @return Le nombre d'individus susceptibles.
     */
    public int getCountS() {
        return this.nbSusceptibles;
    }

    // Autres getters pour les compteurs d'individus dans chaque état
    public int getCountE() {
        return this.nbExposed;
    }

    public int getCountI() {
        return this.nbInfectes;
    }

    public int getCountR() {
        return this.nbRecovered;
    }
}
