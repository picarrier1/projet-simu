
/**
 * La classe Humain représente un individu dans un modèle épidémiologique
 * basé sur les différents statuts d'infection.
 */
public class Humain {

    /**
     * Énumération des différents statuts d'un humain.
     */
    public enum Statuts {
        S, // Susceptible (non infecté)
        E, // Exposé (exposé à l'infection mais pas encore infecté)
        I, // Infecté
        R  // Rétabli (guéri et immunisé)
    }
    
    private Statuts statut; // Statut actuel de l'humain
    private final int dE; // Durée d'incubation (pour passer de E à I)
    private final int dI; // Durée d'infection (pour passer de I à R)
    private final int dR; // Durée de rétablissement (pour passer de R à S)
    private int dS; // Temps écoulé dans le statut actuel
    private int cordx; // Coordonnée x de l'humain dans un système spatial
    private int cordy; // Coordonnée y de l'humain dans un système spatial
    private MTRandom mt; // Générateur de nombres aléatoires pour les durées d'incubation, d'infection et de rétablissement

    /**
     * Constructeur de la classe Humain.
     * @param statut Le statut initial de l'humain.
     * @param mt Générateur de nombres aléatoires.
     * @param tailleMonde La taille du monde.
     */
    public Humain(Statuts statut, MTRandom mt, int tailleMonde) {
        this.mt = mt;
        this.statut = statut;
        this.dS = 0;
        this.dE = initDE();
        this.dI = initDI();
        this.dR = initDR();
        this.newCord(tailleMonde);
    }

    /**
     * Méthode interne pour faire évoluer le statut de l'humain en fonction du temps écoulé.
     */
    private void evolueStatut() {
        switch (this.statut) {
            case E:
                if (this.dS >= this.dE) {
                    this.dS = 0;
                    this.statut = Statuts.I;
                }
                break;
            case I:
                if (this.dS >= this.dI) {
                    this.dS = 0;
                    this.statut = Statuts.R;
                }
                break;
            case R:
                if (this.dS >= this.dR) {
                    this.dS = 0;
                    this.statut = Statuts.S;
                }
                break;
        }
    }

    /**
     * Méthode interne pour initialiser la durée d'incubation.
     * @return La durée d'incubation.
     */
    private int initDE() {
        return (int) (this.mt.negExp(3));
    }

    /**
     * Méthode interne pour initialiser la durée d'infection.
     * @return La durée d'infection.
     */
    private int initDI() {
        return (int) (this.mt.negExp(7));
    }

    /**
     * Méthode interne pour initialiser la durée de rétablissement.
     * @return La durée de rétablissement.
     */
    private int initDR() {
        return (int) (this.mt.negExp(365));
    }

    /**
     * Méthode interne pour mettre à jour le statut de l'individu à "Exposé" (E).
     * Réinitialise également le compteur de temps dans ce statut.
     */
    private void devientE() {
        this.statut = Statuts.E; // Mise à jour du statut de l'individu à "Exposé"
        this.dS = 0; // Réinitialisation du compteur de temps dans le statut "Exposé"
    }

    /**
     * Méthode pour simuler la possibilité qu'un individu devienne exposé à une infection.
     * @param Ni Le nombre d'individus infectés dans le voisinage de cet individu.
     * @return true si l'individu devient exposé à l'infection, false sinon.
     */
    public boolean peutDevenirE(int Ni) {
        // Vérifie si l'individu est déjà exposé à l'infection ou non susceptible
        if (this.statut != Statuts.S) {
            return false;
        }

        // Calcul de la probabilité pour l'individu de devenir exposé à l'infection
        double p = 1 - Math.exp(-0.5 * Ni);

        // Génération d'un nombre aléatoire uniforme entre 0 et 1
        double uniform = mt.genrand_real1();

        // Si le nombre aléatoire est inférieur à la probabilité, l'individu devient exposé à l'infection
        if (uniform < p) {
            this.devientE(); // Appel à la méthode devientE() pour mettre à jour le statut de l'individu
            return true;
        }
        return false; // L'individu ne devient pas exposé à l'infection
    }


    /**
     * Méthode pour incrémenter le temps écoulé dans le statut actuel de l'humain
     * et mettre à jour son statut si nécessaire.
     * @return Le nouveau statut de l'humain après la mise à jour.
     */
    public Statuts incrementTempsStatut() {
        this.dS++; // Incrémente le compteur de temps dans le statut actuel
        evolueStatut(); // Met à jour le statut de l'humain si nécessaire
        return this.statut; // Retourne le nouveau statut de l'humain après la mise à jour
    }

    /**
     * Méthode pour générer de nouvelles coordonnées pour l'humain dans un espace donné.
     * @param tailleMonde La taille du monde.
     */
    public void newCord(int tailleMonde) {
        // Génère des coordonnées aléatoires pour l'axe x et l'axe y dans l'espace donné
        this.cordx = mt.genrand_int32() % tailleMonde;
        this.cordy = mt.genrand_int32() % tailleMonde;
        /*
        System.out.println("x:"+cordx);
        System.out.println("y:"+cordy+"\n");
        */
    }


    /**
     * Méthode pour obtenir la coordonnée x de l'humain.
     * @return La coordonnée x.
     */
    public int getX() {
        return cordx;
    }

    /**
     * Méthode pour obtenir la coordonnée y de l'humain.
     * @return La coordonnée y.
     */
    public int getY() {
        return cordy;
    }

    /**
     * Méthode pour obtenir le statut de l'humain.
     * @return Le statut de l'humain.
     */
    public Statuts getStatut() {
        return statut;
    }

    /**
     * Redéfinition de la méthode toString pour obtenir une représentation textuelle de l'objet Humain.
     * @return Une chaîne de caractères représentant l'objet Humain.
     */
    @Override
    public String toString() {
        return String.format("dE:%d\ndI:%d\ndR:%d\nStatut : %s, Temps dans ce Statut : %d", this.dE, this.dI, this.dR, this.statut, this.dS);
    }

}
