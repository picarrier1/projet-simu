import java.util.ArrayList;
import java.util.List;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Classe principale de l'application de simulation.
 */
public class mainApp {

    private MTRandom mt; // Générateur de nombres aléatoires
    private int tailleMonde; // Taille du monde de la simulation
    private List<Humain> listeHumains; // Liste des humains dans la simulation
    private World world; // Instance du monde de la simulation
    private int nbIndivu;

    /**
     * Méthode principale de l'application.
     * @param args Les arguments de la ligne de commande (non utilisés dans cette application).
     */
    public static void main(String[] args) {
        replicationsExperiences(100, 730); // Appel à la méthode pour exécuter les réplications d'expériences
    }

    /**
     * Méthode pour exécuter des réplications d'expériences.
     * @param nbReplications Le nombre de réplications d'expériences à effectuer.
     * @param nbIteration Le nombre d'itérations par réplication.
     */
    public static void replicationsExperiences(int nbReplications, int nbIteration) {
        long seed = 3207; // Graine pour le générateur de nombres aléatoires
        MTRandom mt = new MTRandom(seed);

        // Itération à travers les réplications
        for (int i = 0; i < nbReplications; i++) {
            System.out.println("\n===== SIMULATION " + (i+1) + " =====");
            mainApp simu = new mainApp(); // Création de l'instance de l'application de simulation
            simu.mt = mt; // Initialisation du générateur de nombres aléatoires
            simu.tailleMonde = 300; // Initialisation de la taille du monde
            simu.nbIndivu = 20000;
            simu.listeHumains = simu.genererListeHumains(19980, 20); // Génération de la liste des humains
            simu.world = new World(simu.tailleMonde, simu.listeHumains); // Création du monde de simulation
            String csvFile = String.format("data/simulation_%d.csv", i + 1); // Nom du fichier CSV
            simu.realiseExperience(nbIteration, csvFile); // Réalisation de l'expérience
        }
    }

    /**
     * Méthode pour réaliser une expérience de simulation.
     * @param nbIteration Le nombre d'itérations de la simulation.
     * @param csvFile Le nom du fichier CSV pour enregistrer les résultats.
     */
    private void realiseExperience(int nbIteration, String csvFile) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(csvFile))) {
            writer.write("moyS, moyE, moyI, moyR\n"); // Écriture de l'en-tête dans le fichier CSV

            double moyS = moyenne(this.world.getCountS(), this.nbIndivu);
            double moyE = moyenne(this.world.getCountE(), this.nbIndivu);
            double moyI = moyenne(this.world.getCountI(), this.nbIndivu);
            double moyR = moyenne(this.world.getCountR(), this.nbIndivu);

            writer.write(moyS+","+moyE+","+moyI+","+moyR+"\n");

            // Itération à travers les jours de la simulation
            for (int i = 0; i < nbIteration; i++) {
                this.world.jourSuivant(); // Progression d'un jour dans la simulation
                //System.out.println("-JOUR " + i + "-");

                moyS = moyenne(this.world.getCountS(), this.nbIndivu);
                moyE = moyenne(this.world.getCountE(), this.nbIndivu);
                moyI = moyenne(this.world.getCountI(), this.nbIndivu);
                moyR = moyenne(this.world.getCountR(), this.nbIndivu);
    
                writer.write(moyS+","+moyE+","+moyI+","+moyR+"\n");
            }

            System.out.println("Fichier CSV créé avec succès !");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static double moyenne(int valeur,int total){
        return (double)(valeur)/(double)(total);
    }
    

    /**
     * Méthode pour générer une liste d'humains avec un nombre spécifié de susceptibles et d'infectés.
     * @param nombreHumainsS Le nombre d'humains susceptibles à générer.
     * @param nombreHumainsI Le nombre d'humains infectés à générer.
     * @return Une liste d'humains contenant des susceptibles et des infectés.
     */
    private List<Humain> genererListeHumains(int nombreHumainsS, int nombreHumainsI) {
        List<Humain> listeHumains = new ArrayList<>();

        // Génération des humains susceptibles
        for (int i = 0; i < nombreHumainsS; i++) {
            Humain nouvelHumain = new Humain(Humain.Statuts.S, this.mt, this.tailleMonde);
            listeHumains.add(nouvelHumain);
        }

        // Génération des humains infectés
        for (int i = 0; i < nombreHumainsI; i++) {
            Humain nouvelHumain = new Humain(Humain.Statuts.I, this.mt, this.tailleMonde);
            listeHumains.add(nouvelHumain);
        }

        return listeHumains;
    }
}
